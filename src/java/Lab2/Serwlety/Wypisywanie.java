/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab2.Serwlety;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import Trwalosc.LudzieQuery;
import Trwalosc.Ludzie;
/**
 *
 * @author student
 */
@WebServlet(name = "Wypisywanie", urlPatterns = {"/Wypisywanie", "/insert"})
public class Wypisywanie extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String inst=request.getParameter("instytucja");
        boolean imie= Boolean.parseBoolean(request.getParameter("name"));
 try (PrintWriter out = response.getWriter()) {
     //new LudzieQuery().executeQuery(insertq);
 out.println("<!DOCTYPE html>");
  out.println("<html>");
 out.println("<head><meta><link rel='stylesheet' href='Style/css/components.css'>");
 out.println("<link rel='stylesheet' href='Style/css/icons.css'>");
 out.println("<link rel='stylesheet' href='Style/css/responsee.css'>");
 out.println("<body>" + "Lista pracowników instytucji<br>"
         + " " + inst + "<br />");
 out.println(new LudzieQuery().getLudzieLista(imie));
 out.println("<a class=\'button rounded-full-btn reload-btn s-2 margin-bottom\' href=");
 out.println(request.getHeader("referer"));
 out.println("><i class='icon-sli-arrow-left'>Powrót</i></a>");
 out.println("</body></html>");

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getServletPath();
        switch (action){
            case "/insert":            
            new LudzieQuery().executeQuery(request.getParameter("IDf"), request.getParameter("imief"),  request.getParameter("nazwiskof"));
            break;
        }
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        String insertq = "insert into ludzie("+request.getParameter("IDf")+","+request.getParameter("imief")+","+request.getParameter("nazwisko");
//        new LudzieQuery().executeQuery(insertq);
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    private boolean polaczenie = false;
 private String data;
 private String getDataFromDb() {
 try {
 polaczenie = Manager.DbManager.Connect();
 if (polaczenie) {
 data = Manager.DbManager.getData();
 polaczenie = Manager.DbManager.Disconnect();
 }
 } catch (ClassNotFoundException | SQLException ex) {
 Logger.getLogger(Wypisywanie.class.getName()).log(Level.SEVERE, null, ex);
 }
 return data;
 }

}
