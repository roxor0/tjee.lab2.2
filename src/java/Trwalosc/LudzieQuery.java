/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Trwalosc;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
/**
 *
 * @author student
 */
public class LudzieQuery {
  private Session session = null;
 private List<Ludzie> ludzieList = null;
 private Query q = null;
 private String Listhtml;
 
 public String getLudzieLista(boolean OrderByImie) {
 
       try {
       org.hibernate.Transaction tx = session.beginTransaction();
       if (OrderByImie) {
       q = session.createQuery("from Ludzie order by imie");
       } else {
       q = session.createQuery("from Ludzie");
       }
       ludzieList = (List<Ludzie>) q.list();
       Listhtml = getListaHTML(ludzieList);
       session.close();
       tx.commit();
       } catch (HibernateException e) {
       }
       return Listhtml;
 }
private String getListaHTML(List<Ludzie> lista) {
        String wiersz;
        wiersz = ("<table><tr>");
        wiersz = wiersz.concat(
        "<td><b>ID</b></td>"
        + "<td><b>IMIE</b></td>"
        + "<td><b>NAZWISKO</b></td>");
        wiersz = wiersz.concat("</tr>");
        for (Ludzie ldz : lista) {
        wiersz = wiersz.concat("<tr>");
        wiersz = wiersz.concat("<td>" + ldz.getId() + "</td>");
        wiersz = wiersz.concat("<td>" + ldz.getImie() + "</td>");
        wiersz = wiersz.concat("<td>" + ldz.getNazwisko() + "</td>");
        wiersz = wiersz.concat("</tr>");
        }
        wiersz = wiersz.concat("</table>");
        return wiersz;
 }
public void executeQuery(String id,String imie, String nazwisko){
    org.hibernate.Transaction tx = session.beginTransaction();
     q = session.createQuery("insert into Ludzie( :id, :imie, :nazwisko )");
    q.setParameter("id",id);
    q.setParameter("imie", imie);
    q.setParameter("nazwisko", nazwisko);
    q.executeUpdate();
    tx.commit();
}      
public LudzieQuery(){
this.session = HibernateUtil.getSessionFactory().getCurrentSession();
}
}
